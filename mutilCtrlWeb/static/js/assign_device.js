// $(document).ready(function(){
function loadAssign_device(user_list_t){
	// user_list_t = {"2": {"username": "user", "is_superuser": false, "first_name": "ljl", "last_name": "lee", "checked": false, "is_staff": false, "is_active": true, "email": "", "date_joined": "2018-04-08 14:14:00"}, 
	// "3": {"username": "ljl", "is_superuser": false, "first_name": "\u674e", "last_name": "\u4f73\u6797", "checked": true, "is_staff": false, "is_active": true, "email": "966484@qq.com", "date_joined": "2018-04-09 14:06:00"}, 
	// "4": {"username": "tim", "is_superuser": false, "first_name": "jl", "last_name": "lee", "checked": true, "is_staff": false, "is_active": true, "email": "966484@qq.com", "date_joined": "2018-04-09 14:13:28"}};
     user_list = getListByJsonType_t(user_list_t);
	$("#table_user_list tr:not(:first)").empty();
	for(var key in user_list){
		var item = user_list[key];
		var isCheck = item[4] == "active" ? "checked=\"true\"":"";
		$("#table_user_list").append("<tr><td>"+item[0].toString()+"</td><td>"+item[1]+"</td></td><td><input type=\"checkbox\" class=\"table_user_list_select\" "+isCheck+"></td></tr>")
	}
	setGloballTableColor();	
}

var user_list = [];
function getListByJsonType_t(type_t){
	list=[];
	for(var key in type_t){
		var item = type_t[key];
		var li=[key, item.username, null,item.email,item.checked?"active":"leaving"];
		list.push(li);
	}
	return list;
}

function setListJsonType_t(li){
	function findListByKey(key){
		for(var idx=0; idx<li.length; idx++){
			if((li[idx])[0] == key)
				return li[idx];
		}
		return null;
	}
	var user_ids=[];
	for(var key in user_list_t){
		var item = user_list_t[key];
		var user_li = findListByKey(key);
		if(user_li != null){
			item.checked = user_li[4] == "active" ? true : false	
			if(user_li[4]=="active"){
				user_ids.push(parseInt(key))
			}		
		}
	}
	return user_ids;
}

function onSelectAllCheckBox() {
	var checkBox = document.getElementById("table_user_list_select_all");
	if(checkBox.checked){
		$(".table_user_list_select").each(function(i,o){
            // $(o).attr("checked",!$(o).attr("checked"));
            $(this).attr("checked","true"); 
        });
	}
	else{
		$(".table_user_list_select").each(function(i,o){
            $(this).removeAttr("checked"); 
        });
	}
}

function onSetDeviceUsers(){
	var checkBox = $(".table_user_list_select");
	for (var key=0; key< checkBox.length; key++){
		var item = user_list[key]
		if(checkBox[key].checked == true){
			item[4] = "active";
		}else{
			item[4] = "laving";
		}		
	}

	var user_ids=setListJsonType_t(user_list);

	// json = {"id":getURLString("id"), "user_ids":user_list}
	$.ajax({
		type: "POST",
		url: "/index/get_user_list/", // http://DESKTOP-T1J5L1V:80
		// data: JSON.stringify(json),
		// contentType : "application/json",
		// dataType : "json",
		data:"id="+getURLString("device_node_id")+"&user_ids="+JSON.stringify(user_ids),
		// beforeSend: function(xhr, settings) { if (!(/^(GET|HEAD|OPTIONS|TRACE|POST)$/.test(settings.type)) && !this.crossDomain) xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));},
		success:function () {
			$("#input_device_users").val(formatCheckedUserList(user_list_t));
			onChangeUser();			
			// self.location=document.referrer;
		}
	});
}
