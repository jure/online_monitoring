$(document).ready(function(){
	var history_datetime = [];
	var values = [];
	var deviceID = 0;
	$("#history_table_tbody").empty();
	try{
		for(var key in history_data){
			deviceID = key;
			var item=history_data[key];
			history_datetime = item.file.datetime;
			values = item.file.value;
		}
	}catch(e){}
	$("#current_page").html(getURLString('page_id'));
	$("#history_title").html(deviceID.toString()+"号设备历史记录");
	$("#input_serach_data").val(getURLString('start'));
	// $("#history_table tr:not(:first)").empty();	

	for(var idx=0; idx<history_datetime.length; idx++){		
		var download_link = "<a class=\"history_link_download\" href=\"/api/download_file/?file_path="+values[idx]+"\">下载</a>";
		$("#history_table_tbody").append("<tr class=\"item_list\"><td>"+history_datetime[idx]+"</td><td>"+values[idx]+"</td><td>"+download_link+"</td></tr>");
	
	}

	 $(".item_list").each(function (idx, item) {
	 	if(idx > 0 && idx%2==0)
	 		$(this).css("background-color"," #BBECDF");
	 	else
	 		$(this).css("background-color","#FFFACD");  
        if(idx==0)
        	$(this).css("background-color","#FFC125");
     });
     

	// $("#history_table_th_date").hover(function(){
	// 	$("#history_table_serach").css("visibility", "visible");
	// }, function(){
	// 	$("#history_table_serach").css("visibility", "hidden");
	// });

	var trs = document.getElementsByClassName('item_list');
    for (var i = 0; i < trs.length; i++)
       	trs[i].onclick = function(){ 
	       	$("tr").each(function (idx, item) {
	           if(idx > 0 && idx%2==0)
	               $(this).css("background-color","#BBECDF");
	           else
	               $(this).css("background-color","#FFFACD");
	    	});
	       	this.style.backgroundColor = "#FFC125";
	       	link_first = this.cells[1].innerText;
       };

});
var link_first="";

function pageClick(n){	
	var rows = parseInt(getURLString('rows'));
	var start = "";
	var trs = document.getElementsByClassName('item_list');
	if(!trs.length || trs.length<=0){
		history.back(-1);
		return;
	}
	direction=0;
	if(n==1) {      // prev_page   				
		if(!!trs.length && trs.length>0) {
	        start = trs[0].cells[0].childNodes[0].textContent;
	        direction=1;
	    }
	}
	if(n==2){ 		
		if(!!trs.length && trs.length>0) {
	        start = trs[trs.length - 1].cells[0].childNodes[0].textContent;
	        direction=0;
	    }
	}	
	window.location.href="/index/get_history_records?start="+start+"&direction="+direction+"&rows="+rows+"&device_node_id="+searchDeviceId()+"&user_id="+searchUserId();
}

function onSerachData(input_start){
	var start = input_start.value.toString(); if(isDate(start)) start+=" 23:59:59"
	var pageID = parseInt(getURLString('page_id'));
	var rows = parseInt(getURLString('rows'));
	window.location.href="/index/get_history_records?start="+start+"&rows="+rows+"&device_node_id="+searchDeviceId()+"&user_id="+searchUserId();
}

function onDownLoadAll(){
	var link_downloads = $(".history_link_download");
	var links = [];	
	var a = document.createElement("a");
	for(var key in link_downloads){
		if(link_downloads[key].href==null || link_downloads[key].href=="#")
			continue;
		var str = link_downloads[key].href;
		if(str.length < 10) continue;
		if(link_first == "") link_first = str.substr(str.indexOf("file_path=")+"file_path=".length);
		var pos = str.indexOf("F:\\AcqCloud\\2018-5-9\\test\\testjango\\");
		if(pos<=0) continue;	
		pos += "F:\\AcqCloud\\2018-5-9\\test\\testjango".length;
		console.log(pos);
		a.href = str.substr(pos);
		links.push(a.href);
	}	
	// console.log("link_first", link_first);
	if(link_first != ""){
		$.ajax({
	        type: "POST",
	        url: "/api/zip_files/", // data: JSON.stringify(json), contentType : "application/json", dataType : "json",
	        data:"zip_path="+link_first,    // beforeSend: function(xhr, settings) { if (!(/^(GET|HEAD|OPTIONS|TRACE|POST)$/.test(settings.type)) && !this.crossDomain) xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));},
	        success:function (data) {
	          try{
	            var obj = eval('(' + data + ')');
	          	if(obj.code = "0"){
	          		var str = "/api/download_file/?file_path="+obj.zip_name;
	          		Download_list("",str);
	          	}
	          }catch(e){console.log("error /api/zip_files/", data);} 
	        }
	    });
	}
	// return;
	// for (let index = 0; index < links.length; index++) {
	// 	console.log(links[index]);
	// 	var thunderLink = ThunderURIEncode(links[index]);
	// 	Download_list(""+index, thunderLink);
	// }
}