from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from mySQL import do as sql
from index import models
from index import dbObject
import json
import simplejson


def login(request):
    if request.method == 'POST':
        username, password, remember = request.POST.get('username', None), request.POST.get('password', None), request.POST.get('remember', None)
        user = dbObject.UserDevicesManage(username, password).user
        if user is not None:
            return HttpResponseRedirect('/index/get_latest_monitor_data/?page_id=1&page_rows=20&user_id=' + str(user.id))
        return render_to_response('error.html', {'message':'没有这个用户或密码错误'})
    return render(request, "login.html")


def get_latest_monitor_data(request):
    device_count, device_list, page_id, page_num, user_id = 1, None, 1, 10, 3
    if request.method == 'GET':
        user_id, page_id, page_rows = int(request.GET.get('user_id', 0)), int(request.GET.get('page_id', 1)), int(request.GET.get('page_rows', 3))
        manage = dbObject.UserDevicesManage(user_id)
        devices = manage.getDevicesAtPage(page_id, page_rows)
        page_num = manage.getDeviceCount() / page_rows
        if page_num > int(page_num):
            page_num = int(page_num) + 1
        dictObj = {'device_list': dbObject.obj_to_dict(devices), 'page_id': page_id, 'page_num': int(page_num), 'user_id': user_id}
        return render_to_response("device.html", dictObj)
    if request.method == 'POST':
        user_id, page_id, page_rows = int(request.GET.get('user_id', 0)), int(request.GET.get('page_id', 1)), int(request.GET.get('page_rows', 3))
        devices = dbObject.UserDevicesManage(user_id).getDevicesAtPage(page_id, page_rows)
        dictObj = {'device_list': dbObject.obj_to_dict(devices), 'page_id': page_id, 'page_num': page_num, 'user_id': user_id}
        return HttpResponse(json.dumps(dictObj))


def get_alarm_list(request):
    if request.method == "GET":
        page_id, rows, device_node_id = 1, int(request.GET.get('rows', 3)), int(request.GET.get('device_node_id', 0))
        device = dbObject.Device(device_node_id).updateSensorsAlarm(page_id, rows)
        return render_to_response("alarm.html", {"snap_list": json.dumps(dbObject.obj_to_dict(device))})
    if request.method == "POST":
        page_id, rows = int(request.POST.get('alarm_id', 1)), int(request.POST.get('points', 20))
    return render(request, "alarm.html")


def get_history_records(request):
    if request.method == 'GET':
        page_id = 1
        rows = int(request.GET.get('rows', 3))
        device_node_id = int(request.GET.get('device_node_id', 0))
        device = dbObject.Device(device_node_id).updateSensorsHistory(page_id, rows)
        return render_to_response("history.html", {"history_data": json.dumps(dbObject.obj_to_dict(device))})
    return render(request, "history.html")


def create_user(request):
    if request.method == "GET":
        allUsers = dbObject.getAllUsersObj()
        return render_to_response("user_center.html", {"user_data": json.dumps(dbObject.obj_to_dict(allUsers))})
    if request.method == "POST":
        userDict = request.POST.get('user', None)
        userDict = simplejson.loads(userDict)
        userDict["id"] = int(userDict["id"])
        dbObject.User(userDict["id"]).updateFromDict(userDict).updateToDatabase()
        return HttpResponse(json.dumps({"result": "true"}))
    return render("user_center.html")


def get_user_list(request):
    if request.method == "GET":
        device_node_id = int(request.GET.get('id', 0))
        users = models.assign_device(device_node_id)
        return render_to_response("assign_device.html", {"user_list": json.dumps(users)})
    if request.method == "POST":
        device_node_id = int(request.POST.get('id', 0))
        user_ids = request.POST.get('user_ids', None)
        user_ids = simplejson.loads(user_ids)
        if user_ids is not None:
            models.attach_user_device(device_node_id, user_ids)
        print(user_ids)
        return HttpResponse(json.dumps({"result": "true"}))
    return render(request, "assign_device.html")


def set_monitor_data(request):
    if request.method == "GET":
        device_node_id = int(request.GET.get('id', 0))
        sensors = sql.allSensorByDid(device_node_id)
        return render_to_response("device_data_format.html", {"sensor_list": json.dumps(sensors)})
    if request.method == "POST":
        device_node_id = int(request.POST.get('id', 0))
        sensor_list = simplejson.loads(request.POST.get('sensor_list', None))  # req = simplejson.loads(request.body)
        if sensor_list is not None:
            models.attach_sensor_device(device_node_id, sensor_list)
        return HttpResponse(json.dumps({"res": "true"}))
    return render(request, "device_data_format.html")
