from django.contrib import admin
from django.urls import path
from django.urls import include
from index import views

urlpatterns = [
    path('login/', views.login),
    path('get_latest_monitor_data/', views.get_latest_monitor_data),
    path('get_alarm_list/', views.get_alarm_list),
    path('get_history_records/', views.get_history_records),
    path('create_user/', views.create_user),
    path('get_user_list/', views.get_user_list),
    path('set_monitor_data/', views.set_monitor_data),
]
