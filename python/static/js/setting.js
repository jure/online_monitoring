 $(function() {
     $("#input_device_id").val(searchDeviceId());
    try{
        $("#input_device_name").val(setting_data.name);
        $("#input_device_type").val(setting_data.type);
         $("#input_device_address").val(setting_data.address);
         $("#input_device_users").val(function (lists){var str="";for(var key in lists)str+=lists[key].name+", ";return str.slice(0,-2);}(setting_data.users));
         $("#input_device_state").val(setting_data.state);
         $("#input_device_description").val(setting_data.description);
         // $("#input_device_range").val(formatSensorArguments(setting_data.sensors));
        setSensor_list(setting_data.sensors);
         $("#button_change_user").attr("href","/index/get_user_list/?id="+searchDeviceId());
          $("#button_change_sensor").attr("href","/index/set_monitor_data/?id="+searchDeviceId());
    }catch (e){console.log(setting_data,e);}
 });

function formatSensorArguments(data){
    var str = "";
    for(var key in data){
        str += data[key].name + ": " +"["+data[key].min +","+ data[key].max +", "+ data[key].unit + "] "+"\r\n";
    }
  // for(var i=0; i<data.length; i++){
  //   str += data[i].name + ": " +"["+data.min[i] +","+ data.max[i] +"] "+ data.unit[i]+"\r\n";
  // }
    return str;
 }

 function onSearchDevice() {
     var inputDevice  = $("#input_device_id").val()
     if(isNull(inputDevice))
         return;
     if(isNumber(inputDevice)) {
         var deviceID = parseInt(inputDevice);
         document.location = "/api/update_device_node/?device_node_id="+deviceID+"&user_id="+serachUserId();
     }
 }
 function setSensor_list(sensor_list){
    $("#table_sensor_list tr:not(:first)").empty();
    function str2td(str_label_id, n){
        return "<input id="+str_label_id+" value="+n.toString()+">"
    }
    for(var key in sensor_list){
        var item = sensor_list[key];
        var name=item.name;var min=item.min;var max=item.max; var unit=item.unit;
        var isCheck = item.state == "active" ? "checked=\"true\"":"";
        var str = "<tr><td>"+name+"</td><td>"+str2td("input_"+name+"_min",min)+"</td><td>"+str2td("input_"+name+"_max",max)+"</td><td>"+str2td("input_"+name+"_unit",unit)+"</td><td><input type=\"checkbox\" class=\"table_sensor_setting_select\" "+isCheck+"></td></tr>"
        $("#table_sensor_list").append(str)
    }
}


// $(document).ready(function(){
//     var sensor_list = setting_data.sensors;
// 	$("#table_sensor_setting tr:not(:first)").empty();
// 	for(var key in sensor_list){
// 		var item = sensor_list[key];
// 		var isCheck = item.state == "active" ? "checked=\"true\"":"";
// 		$("#table_sensor_setting").append("<tr><td>"+item.name+"</td><td>"+item.min+"</td><td>"+item.max+"</td><td>"+item.unit+"</td><td><input type=\"checkbox\" class=\"table_sensor_setting_select\" "+isCheck+"></td></tr>")
// 	}
// 	setGloballTableColor();
// });
function onSelectAllCheckBox() {
	var checkBox = document.getElementById("table_sensor_setting_select_all");
	if(checkBox.checked){
		$(".table_sensor_setting_select").each(function(i,o){
            // $(o).attr("checked",!$(o).attr("checked"));
            $(this).attr("checked","true");
        });
	}
	else{
		$(".table_sensor_setting_select").each(function(i,o){
            $(this).removeAttr("checked");
        });
	}
}