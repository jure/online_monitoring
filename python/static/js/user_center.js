$(document).ready(function(){
	var selectUserObj =document.getElementById('user_center_select_users');
    selectUserObj.length = 0;
    for(var key in user_center) {
    	var item = user_center[key];
    	selectUserObj.options.add(new Option(item.name,item.id));
    }
    selectUserObj.options.add(new Option("创建新用户", "-1"));
	onSelectUser();
});

function onSelectUser() {
 	var sel_obj = document.getElementById("user_center_select_users");
 	var index = sel_obj.selectedIndex;
 	// alert(sel_obj.options[index].value);
 	// alert(sel_obj.options[index].text); 
 	
 	var sel_user = {"state": "active", "name": "", "password": "", "id": 0, "email": null};	// [-1, "", "", "", "", 1]
 	var user_list = [];
 	// user_list = [[3,"张","三","123456","1234@123.com",0],[4,"李","四","13456", "1234@123.com",1],[2,"王","二","13456", "1234@123.com",0]];
 	for(var key in user_center) {
    	 var item = user_center[key];
 	 	user_list.push(item);
	}
 	for (var i = user_list.length - 1; i >= 0; i--) {
 		var item = user_center[i];
 		if(sel_obj.options[index].value == item.id)
 			sel_user = item;
 	}
 	try{ 		
	 	$("#input_user_name").val(sel_user.name);
	 	$("#input_user_password").val(sel_user.password);
	 	$("#input_user_firstname").val("");
	 	$("#input_user_lastname").val(sel_user.name);
	 	$("#input_user_email").val(sel_user.email);
	 	$("#input_user_active").attr("checked", (sel_user.state=="active"?1:0));
 	// $("#input_user_active").val();
 	}catch(e){}
 	
 	// $("#input_user_name").attr("readonly", (sel_obj.options[index].value != "-1"));
 }

 function onChangeName(){
 	$("#input_user_name").val($("#input_user_firstname").val()+$("#input_user_lastname").val());
 }

 function onReload(){onSelectUser();}

 function onSave(){	
    // var first_name = $("#input_user_firstname").val()
    // if(isNull(first_name) || checkQuote(first_name)){
    // 	alert("  姓 \n 输入不合法,检查是否为空或是否包含特殊字符");
    // 	return;
    // }
	var sel_obj = document.getElementById("user_center_select_users");
 	var userID = sel_obj.options[ sel_obj.selectedIndex].value;

    var lastname = $("#input_user_lastname").val();
    if(isNull(lastname) || checkQuote(lastname)){
    	alert("用户名 \n 输入不合法,检查是否为空或是否包含特殊字符");
    	return;
    }   
    var password = $("#input_user_password").val(); 
    if(password.length < 6){
    	alert("  密码 \n 输入不合法, 不能少于6个字符");
    	return;
    }
    if(!isNumberOr_Letter(password)){
    	alert("  密码 \n 输入不合法,只能包含字母数字下划线");
    	return;
    }

    var email = $("#input_user_email").val();
    if(!isEmail(email)) {
    	alert("  邮箱 \n 格式不对");
    	return;
    }
    var checkBox = $("#input_user_active")[0];

    var sel_user = {"state": (checkBox.checked?"active":"leaving"), "name": lastname, "password": password, "id":userID, "email": email};
 	$.ajax({
		type: "POST",
		url: "/index/create_user/",
		// data: JSON.stringify(json),
		// contentType : "application/json",
		// dataType : "json",
		data:"user="+JSON.stringify(sel_user),
		// beforeSend: function(xhr, settings) { if (!(/^(GET|HEAD|OPTIONS|TRACE|POST)$/.test(settings.type)) && !this.crossDomain) xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));},
		success:function () {
			console.log("success");
			location.reload();
			// self.location=document.referrer;
		}
	});
    // alert("通过验证");
}

