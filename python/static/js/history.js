$(document).ready(function() {
   // $(".charts").text(testData());
    try {
        for (var key in history_data.sensors) {
            var itemSensor =  history_data.sensors[key];
            itemSensor.value = [];
            itemSensor.datetime = [];
            for(var key_h in itemSensor.history){
                var itemHistory=itemSensor.history[key_h];
                itemSensor.value.push(itemHistory[2]);
                itemSensor.datetime.push(itemHistory[1]);
            }
        }
    }catch (e){}
    console.log( history_data.sensors)
    dict = setChartsData(history_data, function(dict){for(key in dict)if(key) return key;return '-1';}(history_data));
    pointCount = [];
    if(dict){
        for(var key  in dict){
            var item = dict[key];
            if(item["x"].length > 1)
                pointCount.push(item["x"].length);
            break;
        }
        delete Charts(".charts", dict, pointCount).forEach(function(item){return item.resetQueue().setOption();});
    }
    $("#serach_history_input").val(getURLString("start"));
    // setAlarmLink();
    // setHistoryLink();
});
function getDataTime() {if(history_data)
    for (device in history_data) {
    device_msg = history_data[device];
    for (var key in device_msg)
        if (key == 'datetime')
            return device_msg[key];
    }
    return null;
}
function pageAt(start){
    if(getURLString("rows"))strRows="&rows="+getURLString("rows");else strRows="";
    window.location.href="/index/get_history_records/?device_node_id="+getURLString("device_node_id")+strRows+"&start="+start+"&user_id="+serachUserId().toString();
}

 function nextPage() {
     datatime = getDataTime();
     if(datatime && datatime.length && datatime.length > 0)datatime=datatime[datatime.length-1];
     if(datatime)pageAt(datatime);
 }

function serachPage() {
     var start = $("#serach_history_input").val();
     reg = /^\d\d\d\d-\d(\d)?-\d(\d)?[.-]*/;
     if(start && reg.test(start))
         pageAt(start);
     else
         console.log("!history serachPage reg", start);
}