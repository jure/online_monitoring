var sensor_list = setting_data.sensors;
$(document).ready(function(){
	$("#table_sensor_setting tr:not(:first)").empty();
	for(var key in sensor_list){
		var item = sensor_list[key];
		var isCheck = item[6] == "active" ? "checked=\"true\"":"";
		$("#table_sensor_setting").append("<tr><td>"+item[1]+"</td><td>"+item[3]+"</td><td>"+item[4]+"</td><td>"+item[5]+"</td><td><input type=\"checkbox\" class=\"table_sensor_setting_select\" "+isCheck+"></td></tr>")
	}

	setGloballTableColor();
});
function onSelectAllCheckBox() {
	var checkBox = document.getElementById("table_sensor_setting_select_all");
	if(checkBox.checked){
		$(".table_sensor_setting_select").each(function(i,o){
            // $(o).attr("checked",!$(o).attr("checked"));
            $(this).attr("checked","true");
        });
	}
	else{
		$(".table_sensor_setting_select").each(function(i,o){
            $(this).removeAttr("checked");
        });
	}
}
function onCreateSensor(){
	$("#sensor_setting_create_div").css("visibility", "visible");
}

function onCancelCreate(){
	$("#sensor_setting_create_div").css("visibility", "hidden");
}

function onSetSensorFormat(){
	var checkBox = $(".table_sensor_setting_select");
	for (var key=0; key< checkBox.length; key++){
		var item = sensor_list[key]
		if(checkBox[key].checked == true){
			item[6] = "active";
		}else{
			item[6] = "laving";
		}
		console.log(item)
	}

	json = {"id":getURLString("id"), "user_ids":sensor_list}
	$.ajax({
		type: "POST",
		url: "/index/set_monitor_data/",
		// data: JSON.stringify(json),
		// contentType : "application/json",
		// dataType : "json",
		data:"id="+getURLString("id")+"&sensor_list="+JSON.stringify(sensor_list),
		// beforeSend: function(xhr, settings) { if (!(/^(GET|HEAD|OPTIONS|TRACE|POST)$/.test(settings.type)) && !this.crossDomain) xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));},
		success:function () {
			console.log("success");
			self.location=document.referrer;
		}
	});
}

