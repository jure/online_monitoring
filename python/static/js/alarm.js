$(document).ready(function(){
//     snap_list = {"state": "active", "address": "\u6210\u90fd", "description": null, "id": 1, "datetime": "2018-05-14 15:08:31", "name": "PDV5","sensors":
// [{"state": "active", "id": 1, "max": 1000.0, "value": 72.0, "name": "\u5730\u7535\u6ce2", "datetime": "2018-04-14 15:08:32", "description": null, "unit": "V", "history": [], "min": -1000.0, "d_id": 1, "alarm": "\u6b63\u5e38"},
// {"state": "active", "id": 2, "max": 1000.0, "value": -30.0, "name": "HFCT", "datetime": "2018-05-14 15:10:32", "description": null, "unit": "dB",
//  "history": [[3, "2018-05-14 15:08:32", -30.0]], "min": -1000.0, "d_id": 1, "alarm": "\u6b63\u5e38"}],
//  "users": null, "serial": "PDV20180514A", "type": "\u5728\u7ebf\u76d1\u6d4b"}
    $("#table_alarm_list tr:not(:first)").empty();
	for(var key in snap_list.sensors){
	    var itemSensor = snap_list.sensors[key]
        for(var key_h in itemSensor.history){
	        var itemHistory = itemSensor.history[key_h];
	        console.log(itemHistory)
	        var h_id = itemHistory[0];
	        var d_id = snap_list.id;
	        var s_type = itemSensor.name;
	        var h_time = itemHistory[1];
	        var h_value = itemHistory[2];
	        var h_type = h_value > itemSensor.max ? "过高":"过低";
	        var h_way = "忽视";
	        var h_html = "<tr class='item_list'><td>"+h_id+"</td><td>"+d_id+"</td><td>"+s_type+"</td><td>"+h_time+"</td><td>"+h_value+"</td><td>"+h_type+"</td><td>"+h_way+"</td></tr>";
	        $("#table_alarm_list").append(h_html);
        }
	}
    setGloballTableColor();

    var trs = document.getElementsByClassName('item_list');
   for (var i = 0; i < trs.length; i++)
       trs[i].onclick = trDbClick;
    setAlarmLink();
    setHistoryLink();

   if(trs.length) trs[0].onclick();
   $("#serach_alarm_input").val(getURLString("start"))
 });

var alarm_id=0;
function trDbClick(){
  setGloballTableColor();
  this.style.backgroundColor = "#FFC125";
  alarm_id = parseInt(this.cells[0].childNodes[0].textContent);
  var alarm_data = postData("/index/get_alarm_monitor/", {"id":alarm_id, "points":20}, true)
  try {
        for (var key in alarm_data.sensors) {
            var itemSensor =  alarm_data.sensors[key];
            itemSensor.value = [];
            itemSensor.datetime = [];
            for(var key_h in itemSensor.history){
                var itemHistory=itemSensor.history[key_h];
                itemSensor.value.push(itemHistory[2]);
                itemSensor.datetime.push(itemHistory[1]);
            }
        }
    }catch (e){}
    console.log( alarm_data.sensors)

  var dict = setChartsData(alarm_data   , alarm_id);
  pointCount = 20; if(dict){ for(var key  in dict){var item = dict[key];if(item["x"].length > 1)pointCount = item["x"].length;break;}
  delete Charts(".charts", dict, pointCount).forEach(function(item){return item.resetQueue().setOption();});}
}

 function pageAt(start){
    if(getURLString("rows"))strRows="&rows="+getURLString("rows");else strRows="";
    window.location.href="/index/get_alarm_list?device_node_id="+getURLString("device_node_id")+strRows+"&start="+start;
 }

 function nextPage() {
     trs = document.getElementsByClassName('item_list');
     if(!!trs.length && trs.length>0) {
         pageAt(trs[trs.length - 1].cells[3].childNodes[0].textContent);
     }
 }

function serachPage() {
     var start = $("#serach_alarm_input").val();
     reg = /^\d\d\d\d-\d(\d)?-\d(\d)?[.-]*/;
     if(start && reg.test(start))
         pageAt(start);
     else
         console.log("!alarm serachPage reg", start);
}

function alarmMark(){
  var mark=$("#alarm_mark_input").val();
  if(!mark) alert("不能提交空信息");
  else{
    msg = postData("/index/update_alarm_mark/", {"id":alarm_id, "mark":mark}, true);
    if(!msg) alert("提交出错");
    else{if(parseInt(msg) != 0) alert("错误编码: "+msg);}
  }
}