use monitor; 

DROP TABLE IF EXISTS message;
DROP TABLE IF EXISTS history;
DROP TABLE IF EXISTS sensor;
DROP TABLE IF EXISTS user_device;
DROP TABLE IF EXISTS device;
DROP TABLE IF EXISTS user;

#CREATE TABLE IF NOT EXISTS groups(
#id			INT(0) 			auto_increment primary key,
#name		varchar(20)     unique,
#permission VARCHAR(20) DEFAULT "all"
#);

# CREATE TABLE IF NOT EXISTS user_group(
# id			INT(0)			auto_increment primary key,
# name		varchar(20)		not null,
# g_id		int				default 1,
# foreign key(g_id) 			references permission(id)
# );

CREATE TABLE IF NOT EXISTS user(
 id			INT(0) 			auto_increment primary key,
 name		VARCHAR(20) 	not null unique,
 pass		VARCHAR(20) 	not null,
 email VARCHAR(50),
 state VARCHAR(20) NULL DEFAULT 'active'
# g_id		int				default 1,
# foreign key(g_id) 			references groups(id)
 );
 
create table if not exists device(
id			int(0) 			auto_increment primary key,
name		varchar(20)		not null,
type varchar(20),
address varchar(100),
serial      varchar(20),
state		varchar(20)		default 'active',
others		VARCHAR(50),
time		datetime		default now()
#g_id		int				default 1,
#foreign key(g_id) references groups(id)
);
create table if not exists user_device(
id 		int(0)		auto_increment primary key,
u_id	int			references user(id),
d_id 	int			references device(id)
);

CREATE TABLE IF NOT EXISTS sensor(
id			int(0)			auto_increment primary key,
d_id		int,
name		varchar(20)		not null,
value		float			default 0,
last		datetime		default now(),
unit		varchar(20)		default ' ',
max			float			default 1000,
min			float			default -1000,
description VARCHAR(50),
state  VARCHAR(20) DEFAULT  'active',
foreign key(d_id) references device(id)
);

create table if not exists history(
id			int(0)			auto_increment primary key,
s_id		int				default 1,
time 		Datetime		default now(),
value		float			default 0,
 mark  varchar(20),
foreign key(s_id) references sensor(id)
);

create table if not exists message(
id			int(0)			auto_increment primary key,
d_id		int,
u_id		int,
time		datetime,
text		text,
foreign key(d_id) references device(id),
foreign key(u_id) references user(id)
);

DROP TRIGGER IF EXISTS `monitor`.`history_AFTER_INSERT`;
DELIMITER $$
USE `monitor`$$
CREATE DEFINER = CURRENT_USER TRIGGER `monitor`.`history_AFTER_INSERT` AFTER INSERT ON `history` FOR EACH ROW
BEGIN
	update sensor set value = (select value from history where id=new.id) where id = (select s_id from history where id=new.id);
    update sensor set last=(select time from history where id=new.id) where id = (select s_id from history where id=new.id);
END$$
DELIMITER ;
