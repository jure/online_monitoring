use monitor;

drop view if exists device_view;
create view device_view(u_name, d_id, d_name, d_time, d_state, s_name, s_value, s_time) as
select  user.name, device.id, device.name, device.time, device.state, sensor.name, sensor.value, sensor.last
from device, user, user_device,sensor
where device.id = sensor.d_id and user_device.u_id = user.id and user_device.d_id=device.id;

create view alarm(id,s_id,time,value,mark) as
select h.id, h.s_id, h.time, h.value,h.mark
from history as h,sensor as s
where (h.value<s.min or h.value>s.max) and h.s_id=s.id and(h.mark is null or h.mark <> "ignore");

select * from device_view;

create table if not exists history(
id			int(0)			auto_increment primary key,
s_id		int				default 1,
time 		Datetime		default now(),
value		float			default 0,
foreign key(s_id) references sensor(id)
);