from django.shortcuts import render, render_to_response
from index import dbObject
import json


def update_device_node(request):
    if request.method == "GET":
        device_node_id = int(request.GET.get('device_node_id', 0))
        user_id = int(request.GET.get('user_id', 0))
        device = dbObject.Device(device_node_id)
        device.getUsers()
        device.getAllSensors()
        return render_to_response("seting.html", {"device_obj": json.dumps(device.to_dict())})
    return render(request, "seting.html")
